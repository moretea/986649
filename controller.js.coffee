class Regatta.AppController extends Backbone.Controller
  routes:
    ":controller/:action": "defaultRoute"
  defaultRoute: (controller, action) ->
    Regatta.current.controller = controller
    Regatta.current.action     = action

    viewName = controller + "#" + action
    view = Regatta.view_instances[viewName]
    view.trigger('activated')

    $("#content").html(view.el)