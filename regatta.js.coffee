Regatta =
  Controllers: {}
  Models: {}
  Views: 
    Dashboard: {}
  view_instances: {}
  registerView: (name, instance) ->
    Regatta.view_instances[name] = instance
  current: 
    controller: "dashboard"
    action: "index"
  init: ->
    Regatta.registerView("dashboard#index", new Regatta.Views.Dashboard.Index().render())
   
    Regatta.app_controller = new Regatta.AppController
  
    Backbone.history.saveLocation("dashboard/index") if '' == Backbone.history.getFragment()
    Backbone.history.start()
